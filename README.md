## Dummy API Tests

### Cloning the project

1. Create a project folder on your machine/laptop and browse to the root folder.
2. Clone this project by running this command in your terminal: git clone https://simphiwes@bitbucket.org/twzar/test_api_basalt.git
3. You can then choose to view the project from a text editor of your choice.

### Running Cypress Tests

1.  You can either choose to run the tests from the CLI and run them headlessly by running **npx cypress run**.
2. Run the tests from the cypress UI by running **npx cypress open** and click on the test you want to run and the tests will be run on the electron browser or other browsers eg. Firefox
