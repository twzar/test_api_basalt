describe('http request', () =>{
  it('GET',() =>{
    cy.request({
      method: 'GET',
      url: 'http://httpbin.org/get'
    }).then((response) =>{
      expect(response.body).have.property('url')
    })
  })

  it('POST', () =>{
    cy.request({
      method: 'POST',
      url: 'http://httpbin.org/post',
      body: {
        "name" : "simphiwe",
        "age" : 41
      },
      headers: {
        'content-type' : 'application/json'
      }
    }).then((response) =>{
      expect(response.body.json).to.deep.equal({
        "name" : "simphiwe",
        "age" : 41
      })
    })
  })
})
